﻿
using System.Text;
using DesignPatterns.Composite;
Console.OutputEncoding=Encoding.UTF8;

    var mainHero = new MarvelHero("Peter Parker",150);
    var spiderMan = new MarvelHero("Spider Man",100);
    mainHero.AddFriend(spiderMan);

   // var pureContainer = new ArtefactContainer();
   // mainHero.AddArtefactContainer(pureContainer);

    var stringShooter = new CompositeArtefact("StringShooter",50,25);
    spiderMan.AddArtefact(stringShooter);

    for (int i = 0; i < 5; i++)
    {
        var web = new Artefact("web"+i + 1,10,5);
        stringShooter.AddArtefact(web);
    }
spiderMan.CountArtefacts();
mainHero.RemoveFriend(spiderMan);
Console.ReadKey();
