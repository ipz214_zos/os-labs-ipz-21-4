﻿using System.Text;
Console.OutputEncoding=Encoding.UTF8;
VirusChild[] Viruses = new VirusChild[] { };
var virus=new Virus(89,25,"Ebola","Toxic",Viruses);
var Child1 = new VirusChild(9,4,"Ebola 1.0","Toxic",Viruses,"20.12.2017",virus);
var Child2 = Child1.Clone(13,3,"Ebola 2.0","Toxic",Viruses,"16.09.2018",virus);
var Child3 = Child1.Clone(14,2,"Ebola 3.0","Toxic",Viruses,"13.06.2019",virus);
var Child4 = Child1.Clone(15,1,"Ebola 4.0","Toxic",Viruses,"19.01.2020",virus);

Viruses.Append(Child1);
Viruses.Append(Child2);
Viruses.Append(Child3);
Viruses.Append(Child4);
Child1.About();
Child2.About();
Child3.About();
Child4.About();
virus.About();

class Virus
{
    public float weight;
    public float age;
    public string name;
    public string vid; 
    public VirusChild[] VirusChild1;

    public Virus(float weight,float age, string name,string vid, VirusChild[] virusChild1)
    {
        this.name = name;
        this.vid = vid;
        VirusChild1 = virusChild1;
        this.age = age;
        this.weight = weight;
    }
    public  Virus Clone(float weight,float age, string name,string vid, VirusChild[] virusChild1)
    {
        return new Virus(weight,age,name,vid,VirusChild1);
    }

    public void About()
    {
        Console.WriteLine("Вага:{0}",weight);
        Console.WriteLine("Вік:{0}",age);
        Console.WriteLine("Ім'я:{0}",name);
        Console.WriteLine("Вид:{0}\n\n",vid);
    }


}
class VirusChild:Virus
{
    public string birthDate;
    public Virus parent;
    public VirusChild(float weight, float age, string name, string vid, VirusChild[] virusChild1,string birthDate,Virus parent) : base(weight, age, name, vid, virusChild1)
    {
        this.birthDate = birthDate;
        this.parent = parent;
    }
    public  VirusChild Clone(float weight, float age, string name, string vid, VirusChild[] virusChild1,string birthDate,  Virus parent)
    {
        return new VirusChild(weight,  age,  name,  vid, virusChild1, birthDate,parent);
    }

    public void About()
    {
        Console.WriteLine("Вага:{0}",weight);
        Console.WriteLine("Вік:{0}",age);
        Console.WriteLine("Ім'я:{0}",name);
        Console.WriteLine("Вид:{0}",vid);
        Console.WriteLine("Батько:{0}\n\n",parent);
    }
        
}