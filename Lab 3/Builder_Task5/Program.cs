﻿Console.WriteLine("\nHero\n");
var Hero = new HeroBuilder("Hercules","187","White","Blue","Full set clothes for glory fight","Trawmat,balisong knife, latte machiato");
Hero.About();
Console.WriteLine("\nEnemy\n");
var Enemy = new EnemyBuilder("Gorynych","170","Black","Black","Rvanyie shmotki","Some vodka");
Enemy.About();
interface IBuilder
{
    public string Name { get; set; }
    public string height { get; set; }
    public string hairColor{ get; set; }
    public string EyeColor{ get; set; }
    public string Clothes{ get; set; }
    public string Inventory{ get; set; }
    
}

class HeroBuilder:IBuilder
{
    public string Name { get; set; }
    public string height { get; set; }
    public string hairColor { get; set; }
    public string EyeColor { get; set; }
    public string Clothes { get; set; }
    public string Inventory { get; set; }
    public HeroBuilder(string name, string height, string hairColor, string eyeColor, string clothes, string inventory)
    {
        Name = name;
        this.height = height;
        this.hairColor = hairColor;
        EyeColor = eyeColor;
        Clothes = clothes;
        Inventory = inventory;
    }

    public void About()
    {
        Console.WriteLine($"Name:{Name}");
        Console.WriteLine($"Height:{height}");
        Console.WriteLine($"Hair color:{hairColor}");
        Console.WriteLine($"Eyes color:{EyeColor}");
        Console.WriteLine($"Clothes:{Clothes}");
        Console.WriteLine($"Inventory:{Inventory}");
    }
}

class EnemyBuilder : IBuilder
{
    public string Name { get; set; }
    public string height { get; set; }
    public string hairColor { get; set; }
    public string EyeColor { get; set; }
    public string Clothes { get; set; }
    public string Inventory { get; set; }
    public EnemyBuilder(string name, string height, string hairColor, string eyeColor, string clothes, string inventory)
    {
        Name = name;
        this.height = height;
        this.hairColor = hairColor;
        EyeColor = eyeColor;
        Clothes = clothes;
        Inventory = inventory;
    }
    public void About()
    {
        Console.WriteLine($"Name:{Name}");
        Console.WriteLine($"Height:{height}");
        Console.WriteLine($"Hair color:{hairColor}");
        Console.WriteLine($"Eyes color:{EyeColor}");
        Console.WriteLine($"Clothes:{Clothes}");
        Console.WriteLine($"Inventory:{Inventory}");
    }
}